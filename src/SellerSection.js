import React, { Component } from 'react';
import SellerThumbnail from './SellerThumbnail.js';
import {Col} from 'react-bootstrap';
import $ from 'jquery';
export default class SellerSection extends Component {

  constructor(props){
    super(props);
    this.state={
    };
  };
  getSellers() {
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://94.130.149.100:4000/category/"+this.props.category_id+"/seller",
      "method": "GET",
      "headers": {
        "sub_category_id": this.props.subcategory_id
      }
    }
    $.ajax(settings).done( (response) => {
      console.log(response.body);
      this.setState({sellers: response.body})
    });
  }
  componentWillMount() {
    this.getSellers();
  }
  render() {
    if(this.state.sellers)
    {
      var sellers = this.state.sellers.map( (item, index) => {
          if(index === 0)
          {
            return(<SellerThumbnail sellerName={item.name} isLarge={true}/>)
          }
          else{
            return(<SellerThumbnail sellerName={item.name}/>)
          }
      });
    }
    return (
      <div className="sellersection">
        <Col xsOffset={1} smOffset={1} mdOffset={1} lgOffset={1} xs={7} sm={7} md={8} lg={8}>
          {sellers}
          {/* <SellerThumbnail isLarge={true}/>
          <SellerThumbnail/>
          <SellerThumbnail/><SellerThumbnail/>
          <SellerThumbnail/><SellerThumbnail/>
          <SellerThumbnail/><SellerThumbnail/>
          <SellerThumbnail/><SellerThumbnail/>
          <SellerThumbnail/><SellerThumbnail/>
          <SellerThumbnail/><SellerThumbnail/>
          <SellerThumbnail/><SellerThumbnail/>
          <SellerThumbnail/> */}
        </Col>
      </div>
    )
  }
}
