import React, { Component } from 'react';
import {Panel, ListGroup, ListGroupItem} from 'react-bootstrap';
import $ from 'jquery';
import { Link } from "react-router-dom";
export default class ListContainer extends Component {
  
  constructor(props){
    super(props);
    this.state={
    };
    this.getSubcategories = this.getSubcategories.bind(this);
  };
  getSubcategories() {
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://94.130.149.100:4000/category/"+this.props.categoryId+"/child",
      "method": "GET"
    }
    
    var results;
    $.ajax(settings).done( (response) => {
      results = response.body;
      this.setState({subcategories: results});
      //console.log(this.state.subcategories);
      this.forceUpdate();
    });
  }
  componentWillMount() {
    this.getSubcategories(); 
  }
  render()
  {
    var subcategoryListItems;
    if(this.state.subcategories)
    {
      subcategoryListItems = this.state.subcategories.map( (item, index) => {
        return(
          <ListGroupItem key={index}>{item.name}</ListGroupItem>
        )
      });
    }
    else {
      //this.getSubcategories();
    }
    return (
      <div className="listcontainer">
        <Panel>
          <Panel.Heading>
            <Panel.Title componentClass="h3"><Link to={"category/"+this.props.title}>{this.props.title}</Link></Panel.Title>
          </Panel.Heading>
          <ListGroup>
          <Panel.Body>
            {subcategoryListItems}
          </Panel.Body>
          </ListGroup>
        </Panel>
      </div>
    )
  }
}
