import React, { Component } from 'react';
import {Image} from 'react-bootstrap';
import logo from "./empty.jpg"
export default class ProductThumbnail extends Component {
  constructor(props){
    super(props);
    this.state={
      size: "easy",
    };
    this.setSize = this.setSize.bind(this);
  };
  componentWillMount() {
    this.setSize();
  }
  componentDidMount() {
    this.setSize();
  }
  setSize() {

    if(this.props.isLarge)
    {
      this.setState({size: "300px"});
    }
    else
    {
      this.setState({size: "100px"});
    }
  }
  render() {
    return (
      <div className="productthumbnail">
        <Image thumbnail responsive src={logo} alt="Easy" style={{width: this.state.size, height: this.state.size}}/>
      </div>
    )
  }
}
