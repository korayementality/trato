import React, { Component } from 'react';
import {Thumbnail, Col} from 'react-bootstrap';
import logo from './empty.jpg';
export default class SellerThumbnail extends Component {
  constructor(props){
    super(props);
    this.state={
      size: "easy",
    };
    this.setSize = this.setSize.bind(this);
  };
  componentWillMount() {
    this.setSize();
  }
  componentDidMount() {
    this.setSize();
  }
  setSize() {

    if(this.props.isLarge)
    {
      this.setState({size: "300px"});
    }
    else
    {
      this.setState({size: "100px"});
    }
  }
  render() {
    return (
      <div className="sellerthumbnail">
      <Col xs={this.props.isLarge? 6 : 3} sm={this.props.isLarge? 6 : 3} md={this.props.isLarge? 6 : 3} lg={this.props.isLarge? 6 : 3}>
        <Thumbnail src={logo} alt="Easy" style={{width: this.state.size, height: (this.state.size/2)}}><h4>{this.props.sellerName}</h4></Thumbnail>
      </Col>
      </div>
    )
  }
}
