import React, { Component } from 'react';
import {Panel, Thumbnail} from 'react-bootstrap';
import logo from './empty.jpg';
export default class FeatureContainer extends Component {
  constructor(props){
    super(props);
    this.state={
    };
  };
  render() {
    return (
      <div className="featurecontainer">
        <Panel>
          <Panel.Heading>
            <Panel.Title componentClass="h3">{this.props.title}</Panel.Title>
          </Panel.Heading>
            <Thumbnail src={logo}/>
        </Panel>
      </div>
    )
  }
}
