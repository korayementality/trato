import React, { Component } from 'react';
import HomeCarousel from './HomeCarousel.js'
import './App.css';
// eslint-disable-next-line
import {Grid, Row, Col, Carousel} from 'react-bootstrap';
import ListContainer from './ListContainer.js';
import CategoryDisplay from './CategoryDisplay.js';
import FeatureContainer from './FeatureContainer.js';
import $ from 'jquery';
export default class Home extends Component {
  constructor(props){
    super(props);
    this.state={
      categories: [{}]
    };
    this.getCategories = this.getCategories.bind(this);
  };

  getCategories() {
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://94.130.149.100:4000/category",
      "method": "GET"
    }
    
    var results;
    $.ajax(settings).done( (response) => {
      //console.log(response.body);
      results = response.body;
      this.setState({categories: results});
    });
  }
  componentDidMount() {
    this.getCategories();
    
  }
  render() {
    var categoryArray = this.state.categories;
    var categoryDisplayList = categoryArray.map( (item, index) => {
      return(
      <CategoryDisplay
        key={index}
        title={item.name} 
        categoryId={item._id}
      />)
    });
    return (
      <div className="home">
        <HomeCarousel></HomeCarousel>
            <Grid fluid={true}>

              <Row style={{backgroundColor : 'red'}}> 
                <Col xs={3} sm={3} md={3} lg={3} ><a>Home</a></Col>
                <Col xs={3} sm={3} md={3} lg={3}><a>Feeds</a></Col>
                <Col xs={3} sm={3} md={3} lg={3}><a>About Trato</a></Col>
                <Col xs={3} sm={3} md={3} lg={3}><a>Customer Service</a></Col>
              </Row>
              <Row>
                <Col xsOffset={1} smOffset={1} mdOffset={1} lgOffset={1} xs={5} sm={5} md={5} lg={5}>
                  <FeatureContainer title="New Entry"/>
                </Col>
                <Col xs={5} sm={5} md={5} lg={5}>
                  <FeatureContainer title="Featured Brand"/>
                </Col>
              </Row>
              <Row>
                <Col xs={8} sm={8} md={7} lg={7} mdOffset={1} lgOffset={1}>
                {categoryDisplayList}
                </Col>
                  <Col xs={4} sm={4} md={3} lg={3}>
                    <Row>
                      <ListContainer 
                        title="Best Sellers" 
                        subcategory1="McDonald's" 
                        subcategory2="KFC"
                        subcategory3="Papa John's"
                        subcategory4="Cook Door"/>
                    </Row>
                    <Row>
                      <ListContainer 
                        title="Most Used Offers" 
                        subcategory1="McDonald's" 
                        subcategory2="KFC"
                        subcategory3="Papa John's"
                        subcategory4="Cook Door"/>
                    </Row>
                  </Col>
                </Row>
            </Grid>
      </div>
    )
  }
}
