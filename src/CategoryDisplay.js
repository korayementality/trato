import React, { Component } from 'react';
// eslint-disable-next-line
import {Grid, Row, Col} from 'react-bootstrap';
import ProductThumbnail from './ProductThumbnail.js';
import ListContainer from './ListContainer.js';
export default class CategoryDisplay extends Component {
  constructor(props){
    super(props);
    this.state={
    };
  };

  render() {
    return (
      <div className="categorydisplay">
        <Row>
          <Col xs={7} sm={3} md={3} lg={3}>
            <ListContainer 
            title={this.props.title} 
            categoryId={this.props.categoryId}
            />
          </Col>
          <Col xs={3} sm={3} md={6} lg={6}>
            <ProductThumbnail isLarge={true}/>
          </Col>
          <Col xs={2} sm={3} md={3} lg={3}>
            <Row>
              <ProductThumbnail isLarge={false}/>
            </Row>
            <Row>
              <ProductThumbnail isLarge={false}/>
            </Row>
            <Row>
              <ProductThumbnail isLarge={false}/>
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}
