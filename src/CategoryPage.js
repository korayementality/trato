import React, { Component } from 'react';
import {Row, Col, Thumbnail, Label, Tab, Tabs, Pagination} from 'react-bootstrap';
import logo from './empty.jpg';
import ListContainer from './ListContainer.js';
import FeatureContainer from './FeatureContainer.js';
import SellerSection from './SellerSection.js';
import $ from 'jquery';
export default class CategoryPage extends Component {

  constructor(props){
    super(props);
    this.state={
      categories: [{}],
      currentCategory: {},
      subcategories: [{}]
    };
    this.getCategories = this.getCategories.bind(this);
    this.getSubcategories = this.getSubcategories.bind(this);
  };

  getCategories() {
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://94.130.149.100:4000/category",
      "method": "GET"
    }
    
    var results;
    $.ajax(settings).done( (response) => {
      //console.log(response.body);
      results = response.body;
      this.setState({categories: results});
      for(var i = 0; i < this.state.categories.length; i++)
      {
        if(this.state.categories[i].name === this.props.match.params.categoryId)
        {
          this.setState({currentCategory: this.state.categories[i]});
          break;
        }
      }
      this.getSubcategories();
    });
  }
  getSubcategories() {
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://94.130.149.100:4000/category/"+this.state.currentCategory._id+"/child",
      "method": "GET"
    }
    
    var results;
    $.ajax(settings).done( (response) => {
      results = response.body;
      this.setState({subcategories: results});
    });
  }
  componentWillMount() {
    this.getCategories(); 
  }

  render() {
    var tabs = this.state.subcategories.map( (item, index) => {
      return(
      <Tab key={index} eventKey={index} title={item.name}>
        <SellerSection category_id={this.state.currentCategory._id} subcategory_id={item._id}/>
      </Tab>
      )
    })
    return (
      <div className="categorypage">
        <Row style={{backgroundColor : 'red'}}> 
          <Col xs={3} sm={3} md={3} lg={3} ><a>Home</a></Col>
          <Col xs={3} sm={3} md={3} lg={3}><a>Feeds</a></Col>
          <Col xs={3} sm={3} md={3} lg={3}><a>About Trato</a></Col>
          <Col xs={3} sm={3} md={3} lg={3}><a>Customer Service</a></Col>
        </Row>
        <Row style={{paddingTop: "25px"}}>
          <Col xs={4} sm={4} md={4} lg={4}>Home > Categories > {this.props.match.params.categoryId}</Col>
        </Row>
        <Row style={{paddingTop: "25px"}}>
          <Col xsOffset={1} smOffset={1} mdOffset={1} lgOffset={1} xs={2} sm={2} md={2} lg={2}>
            <div className="categorypagename">
              <h1><Label className="categorypagelabel" bsStyle="success">{this.props.match.params.categoryId}</Label></h1>
            </div>
          </Col>
          <Col xs={4} sm={4} md={4} lg={4}>
           <Thumbnail src={logo}/>
          </Col>
          <Col xs={4} sm={4} md={4} lg={4}>
            <Thumbnail src={logo}/>
          </Col>
        </Row>
        <hr/>
        <Row>
          <Col xsOffset={1} smOffset={1} mdOffset={1} lgOffset={1}>
            <Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
              {tabs}
              
            </Tabs>
          </Col>
            
          <Col  xs={4} sm={4} md={2} lg={2}>
          <Row>
            <ListContainer 
              title="Best Sellers" 
              subcategory1="McDonald's" 
              subcategory2="KFC"
              subcategory3="Papa John's"
              subcategory4="Cook Door"/>
          </Row>
          <Row>
            <ListContainer 
              title="Most Used Offers" 
              subcategory1="McDonald's" 
              subcategory2="KFC"
              subcategory3="Papa John's"
              subcategory4="Cook Door"/>
          </Row>
          </Col>
        </Row>
        <Row>
        <Pagination>
          <Pagination.First />
          <Pagination.Prev />
          <Pagination.Item>{1}</Pagination.Item>
          <Pagination.Ellipsis />

          <Pagination.Item>{10}</Pagination.Item>
          <Pagination.Item>{11}</Pagination.Item>
          <Pagination.Item active>{12}</Pagination.Item>
          <Pagination.Item>{13}</Pagination.Item>
          <Pagination.Item disabled>{14}</Pagination.Item>

          <Pagination.Ellipsis />
          <Pagination.Item>{20}</Pagination.Item>
          <Pagination.Next />
          <Pagination.Last />
        </Pagination>
        </Row>
        <Row>
          <Col xs={3} sm={3} md={3} lg={3}></Col>
          <Col xs={6} sm={6} md={6} lg={6} >
            <Thumbnail src={logo}/>
          </Col>
          <Col xs={3} sm={3} md={3} lg={3}></Col>
        </Row>
        <Row>
          <Col xsOffset={1} smOffset={1} mdOffset={1} lgOffset={1} xs={5} sm={5} md={5} lg={5}>
            <FeatureContainer title="New Entry"/>
          </Col>
          <Col xs={5} sm={5} md={5} lg={5}>
            <FeatureContainer title="Featured Brand"/>
          </Col>
        </Row>
      </div>
    )
  }
}
