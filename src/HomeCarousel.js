import React, { Component } from 'react';
// eslint-disable-next-line
import {Grid, Row, Col, Carousel, Thumbnail} from 'react-bootstrap';
import './App.css';
import logo from './empty.jpg';

class HomeCarousel extends Component {
  render() {
    return (
      <Carousel>
          <Carousel.Item>
            <Thumbnail width={400} height={400} alt="900x500" src={logo} />
            <Carousel.Caption>
              <h3>First slide label</h3>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <Thumbnail width={400} height={400} alt="900x500" src={logo} />
            <Carousel.Caption>
              <h3>Second slide label</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <Thumbnail width={400} height={400} alt="900x500" src={logo} />
            <Carousel.Caption>
              <h3>Third slide label</h3>
              <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
            </Carousel.Caption>
          </Carousel.Item>
    </Carousel>
    );
  }
}

export default HomeCarousel;
